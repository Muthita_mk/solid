using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    [SerializeField] private AttackController _attackController;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.TryGetComponent<IDamageable>(out var damageable) && _attackController.gameObject != collider.gameObject)
            damageable.ApplyDamage( gameObject, _attackController.GetAttackDamage());
    }
}