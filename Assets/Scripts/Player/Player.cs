using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour, IDamageable
{
    public UnityEvent<int> onHpChanged;

    private const int MaxHp = 5;

    [Header("Movement")]
    [SerializeField] private Rigidbody2D rb;

    [Header("Health")] 
    [SerializeField] private int currentHp = MaxHp;
    
    [Header("Attack")]
    [SerializeField] private Transform spawnPoint;
    

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }
    
    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    
    public void DecreaseHp(int _value)
    {
        currentHp -= _value;
        
        if (currentHp <= 0)
        {
            Death();
        }
        onHpChanged?.Invoke(currentHp);
    }

    private void Death()
    {
        currentHp = MaxHp;
        Respawn();
    }

    private void Respawn()
    {
        rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }

    public void ApplyDamage(GameObject _source, int _damage)
    {
        DecreaseHp(_damage);
    }

    #endregion

}
